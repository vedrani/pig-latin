import toPigLatin from './pigLatin';

window.onload = () => {
    const input = document.getElementById('input') as HTMLTextAreaElement;
    const output = document.getElementById('output') as HTMLTextAreaElement;

    input.addEventListener('keyup', (e: KeyboardEvent) => {
        if (input.value) {
            const translatedWord = toPigLatin(input.value);
            output.value = translatedWord;
        } else {
            output.value = '';
        }
    });
};
