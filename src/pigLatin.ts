const VOWELS = ['a', 'e', 'i', 'o', 'u'];
const PUNCTUATIONS = ['.', ',', ':', ';', '?', '(', ')', '[', ']', '"', "'", '’', '!', '/', '\\', '@', '_'];

type sFn = (word: string) => string;

const endsWithWay = (word: string) => word.endsWith('way');
const startsWithVowel = (word: string) => VOWELS.includes(word[0]);
const startsWithChar = (word: string) => word[0] >= 'A' && word[0] <= 'z';
const addWayToEnd = (word: string) => word + 'way';
const addAyToEnd = (word: string) => word + 'ay';
const isPunctuation = (char: string) => PUNCTUATIONS.includes(char);
const isCapitalLetter = (char: string) => char >= 'A' && char <= 'Z';
const capitalizeAtIndex = (word: string, index: number) =>
    word.substring(0, index)
    + word[index].toUpperCase()
    + word.substring(index + 1, word.length);
const addCharAtIndex = (word: string, char: string, index: number) =>
    word.substring(0, index)
    + char
    + word.substring(index, word.length);

export const putFirstToEnd = (word: string) => word.substring(1) + word.substring(0, 1);

export const keepUpperCase = (fn: sFn) => (word: string) => {
    const upperCaseIndexes = [];
    for (let i = 0; i < word.length; i++) {
        if (isCapitalLetter(word[i])) {
            upperCaseIndexes.push(i);
        }
    }

    let newWord = fn(word.toLowerCase());

    while (upperCaseIndexes.length) {
        const upperCaseIndex = upperCaseIndexes.shift();
        newWord = capitalizeAtIndex(newWord, upperCaseIndex || 0);
    }

    return newWord;
};

export const keepPunctuation = (fn: sFn) => (word: string) => {
    const punctuations = [];
    for (let i = 0; i < word.length; i++) {
        if (isPunctuation(word[i])) {
            punctuations.push({ index: word.length - i - 1, symbol: word[i] });
        }
    }

    for (const p of punctuations) {
        word = word.replace(p.symbol, '');
    }

    let newWord = fn(word);

    while (punctuations.length) {
        const punctuation = punctuations.pop();
        newWord = addCharAtIndex(newWord, punctuation?.symbol || '', newWord.length - (punctuation?.index || 0));
    }

    return newWord;
};

const translateWordBase = (word: string) => {
    if (endsWithWay(word)) {
        return word;
    }

    if (startsWithVowel(word)) {
        return addWayToEnd(word);
    }

    if (!startsWithChar(word)) {
        return word;
    }

    return addAyToEnd(putFirstToEnd(word));
};

const skipEmpty = (fn: sFn) => (word: string) => {
    if (word === ' ' || word === '') {
        return word;
    }

    return fn(word);
};

export const translateWord = skipEmpty(keepPunctuation(keepUpperCase(translateWordBase)));

export const splitBySpaceAndHyphen = (fn: sFn) => (text: string) => {
    const splitByDash = (word: string) => word.split('-').map(fn).join('-');
    const splitByNewLine = (word: string) => word.split('\n').map(splitByDash).join('\n');
    return text.split(' ').map(splitByNewLine).join(' ');
};

export default splitBySpaceAndHyphen(translateWord);
