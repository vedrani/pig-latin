import { putFirstToEnd, translateWord, keepUpperCase, keepPunctuation, splitBySpaceAndHyphen } from '../pigLatin';

const addA = (word: string) => word + 'a';
const addB = (word: string) => 'b' + word;

describe('putFirstToEnd', () => {
    it('should put first letter to the end', () => {
        expect(putFirstToEnd('hello')).toEqual('elloh');
        expect(putFirstToEnd('h')).toEqual('h');
    });
});

describe('keepUpperCase', () => {
    it('should keep upper case on same place', () => {
        expect(keepUpperCase(addA)('Hello')).toEqual('Helloa');
        expect(keepUpperCase(addA)('McCloud')).toEqual('McClouda');
        expect(keepUpperCase(addA)('McClouD')).toEqual('McClouDa');
        expect(keepUpperCase(addB)('Hello')).toEqual('Bhello');
        expect(keepUpperCase(addB)('McCloud')).toEqual('BmCcloud');
    });
});

describe('keepPunctuation', () => {
    it('should keep punctuation on their place from the end', () => {
        expect(keepPunctuation(addA)('bla?')).toEqual('blaa?');
        expect(keepPunctuation(addA)('b_la?')).toEqual('bl_aa?');
        expect(keepPunctuation(addB)('bla?')).toEqual('bbla?');
    });
});

describe('translateWord', () => {
    it('should translate to pigLatin', () => {
        expect(translateWord('apple')).toEqual('appleway');
        expect(translateWord('Hello')).toEqual('Ellohay');
        expect(translateWord('Beach')).toEqual('Eachbay');
        expect(translateWord('McCloud')).toEqual('CcLoudmay');
        expect(translateWord('stairway')).toEqual('stairway');
        expect(translateWord('can’t')).toEqual('antca’y');
        expect(translateWord('end.')).toEqual('endway.');
        expect(translateWord('Hell.o')).toEqual('Elloha.y');
        expect(translateWord('Hell.o')).toEqual('Elloha.y');
        expect(translateWord('=back')).toEqual('=back');
        expect(translateWord('HelL(o')).toEqual('EllOha(y');
        expect(translateWord('hell(O')).toEqual('elloHa(y');
    });
});

describe('splitBySpaceAndHyphen', () => {
    it('should split text by space and words by dash then run given function', () => {
        const sentence = 'hello this-thing McCloud';
        const expectedSentence = 'ellohay histay-hingtay CcLoudmay';
        expect(splitBySpaceAndHyphen(translateWord)(sentence)).toEqual(expectedSentence);
    });
});
